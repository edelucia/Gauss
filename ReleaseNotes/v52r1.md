

2018-06-11 Gauss v52r1
===

This version uses LHCb **v44r2p1**, Geant4 **v104r1p1**, Gaudi v29r4 and LCG 93 with
 HepMC 2.06.09 and Root 6.12.06

The generators used via LCG_93/MCGenerators) are
 pythia8 **235** (with LHCb Tune1), lhapdf **6.1.6.cxxstd**, photos++ 3.56, 
 tauola++ 1.1.6b.lhcb, pythia 6.427.2,
 hijing 1.383bs.2, crmc 1.5.6 (epos), alpgen 2.1.4, powhegbox **r3043.lhcb**, 
 herwig++ 2.7.1, thepeg **2.1.1**, 
 rivet **2.6.0**, yoda **1.6.7**.
 **startlight r300**  
and the internal implementation of:
 EvtGen with EvtGenExtras, AmpGen, Mint,
 BcVegPy and GenXicc, **SuperChic2**, LPair, MIB

The data packages specific for the simulation are 
Geant4Files v104r* , GDMLData v1r* , XmlVis v2r* , DecFiles v30r* , 
LHAPDFSets v2r* , BcVegPyData v2r* , GenXiccData v3r* , PGunsData v1r* , 
MIBData v3r*


<p>
This version is released on `master`. 
<p>
It is to be used for productions referred to as **Sim10Dev** and is for tuning, 
setting up and cross check in view of Sim10.

### New features

#### Generators

- Move to latest pythia8 version released end of March 2018, !274 (@gcorti)   
  See Pythia 8.235 in the [Update History of Pythia at http://home.thep.lu.se/~torbjorn/pythia82html/UpdateHistory.html

- LHCBGAUSS-815 SuperChic2, !300 (@gcorti) [LHCBGAUSS-815]  
  Merge branch 'cherry-pick-972a8296' into 'Sim09-upgrade'   
  See merge request lhcb/Gauss!295 that was already cherry pick of lhcb/Gauss!94 for 'Sim09' but has additional commits (see details below)  
  (cherry picked from commit bf9841889025fa41717f6fe5e39cef3ba660c9ab)    
  28be1db1 Merge branch 'LHCBGAUSS-815.SuperChic2' into 'Sim09'     
  896d7efe remove cmt from SuperChi2     
  dcf81ae1 remove left over kumac in cmt directory    
  b43bc1f5 remove obsolete first version of SuperChic    
  Closes LHCBGAUSS-815  

- LHCBGAUSS-923.StarLight.master, !294 (@philten) [LHCBGAUSS-923]  
  Porting StarLight interface for inclusion with master.  
  This replaces !111 where the whole history of changes can be found

#### Detector Simulation

- Compatibility changes to adapt to G4r10, !113 (@dpopov)   
  The changes to Gauss and sub-packages which are required to work with the new version of G4 (v10r3p2).


- Port filter on gamma conversion to master, !304 (@gcorti)   
  This correspond to merge request !203 in `Sim09` and !242 in `Sim09-upgrade`        
  Cherry-pick had conflicts in Sim/Gauss/python/Gauss/Configuration.py on the explanation of the new properties. The  
  cherry pick was done commit by commit and resolved for Configuration.py by editing by hand.

- Add FTFP_BERT_HP to Sim10, !301 (@dpopov) [LHCBGAUSS-1401]  
  This MR is an adaptation of !299 with changes necessary for `master/Sim10`.      
  Relevant JIRA ticket: `LHCBGAUSS-1401`

- EPOS.py mod.: light particle event types not using pythia, but EPOS, !261 (@gcorti) [LHCBGAUSS-1320]  
  Port merging of branch 'LHCBGAUSS-1320.EposforLFevttypes' into 'Sim09'  
  See merge request lhcb/Gauss!259, also correspond to lhcb/Gauss!262  
  (cherry picked from commit 9616f3af6da207f6f6a6dd5af38f7cc02ed8cbe8)  
  f44a9d35 EPOS.py mod.: light particle event types not using pythia, but EPOS

- gamma-jet for EPOS, !239 (@gcorti) [LHCBGAUSS-1254]  
  See merge request lhcb/Gauss!232, i.e. merge branch 'cherry-pick-2bfc4676' into 'Sim09-upgrade'  
  (cherry picked from commit d9e7bdff469a30887155666fefe5540da719b96c)  
  f6e607f4 Merge branch 'LHCBGAUSS-1254.gammajetforEPOS' into 'Sim09'

- New EvtGen models for chi_cJ -> psi mu mu decays, !237 (@gcorti)   
  See merge request lhcb/Gauss!230   
  (cherry picked from commit 14fac1e7b6976e06dd70522dc9993107b6157dda) i.e. 'cherry-pick-7d0ebd3b' into 'Sim09-upgrade' of  
  961a2167 Merge branch 'chic_models_v49r8' into 'Sim09' (MR !207)


### Enhancements

#### Generators

- Tackling slow pythia event types: Applying (part of) the cut tool in modified EvtGenDecay, !305 (@gcorti)   
  Porting merge of branch 'dmuller-EvtGenDecayWithCutTool'  
  Cherry pick 47e3f919 from MR !292 for 'Sim09-upgrade' that was cherry-pick by hand of merge request lhcb/Gauss!266 for 'Sim09'

- GenCut changes for R(D*) hadronic, !291 (@gcorti) [LHCBGAUSS-1323]  
  Porting merge of branch 'LHCBGAUSS-1323.DaughtersInLHCbAndCutsForDstarXInheritance'  
  See merge request lhcb/Gauss!258 for 'Sim09' and lhcb/Gauss!290 for 'Sim09-upgrade'  
  (cherry picked from commit 116beb2a3d2f98b5c1d4a99cfdbb3569e8248666)  
  3ecb213c inherit from DaughtersInLHCb    
  2cf67e9b inherit from DaughtersInLHCbAndCutsForDstar, split main function into several digestible steps    
  353f693a inherit from DaughtersInLHCbAndCutsForDstar3pi    
  80fdb63e inherit from DaughtersInLHCbAndCutsForDstarD    
  ba7c83d6 migrate stable b-hadron checks to static members    
  d76bb8c5 change inheritance, add vertex check, rm unnecessary check    
  a5e4342f add vertex check, fix logic    
  e6a143cb replace loops with algorithms. remove need for vertex check by only looking at…    
  1c4b6538 Remove inheritance from ForDstar3pi to ForDstar. Restore identical cuts.    
  a8f4357f wrap debug messages. simpler D0 lifetime calculation    
  e0f88a3a stable_b_hadrons vector->set, is_it_stable_b_had as regalar function    
  155894b1 check for extra D's in any descendants of the D*-parent b-hadron    
  545ba8a4 inherit from DstarD again    
  24d324df Equate vertices by barcode not pointers. Doxygen comments.    
  66e91681 add debug messages, change D* descendant veto in…    
  03e910c7 Decay other D* particles. Fix bug in min pT calculation. Delegate b-hadron check…    
  293029aa Change to IFullGenEventCutTool as parent class    
  e1d1ac0c complete rewrite: these cuts can be split into stages (Cuts and FullEventCuts)    
  4e299cad New class: ExtraParticlesInAcceptance    
  5d98c91b debug statements    
  3ed33128 documentation in declareProperty    
  2b068f04 move call of hasAncestor() to make passesCuts() more readable. improve documentation    
  104edfd3 add ability to exclude signal decay products and require all particles to be from same B decay    
  48abfb0d Changes in response to comments    
  68a5e809 declare component

- LHCBGAUSS-1351 Updating particle properties for EvtGen Pythia decay model, !289 (@gcorti) [LHCBGAUSS-1351]  
  Port marging of branch 'LHCBGAUSS-1351' into 'master'    
  See merge request lhcb/Gauss!267 for 'Sim09' and lhcb/Gauss!288 for 'Sim09-upgrade'  
  (cherry picked from commit d405d119a8bcbefdd6e5f98d40d807f83f91cff5)  
  9389f4a0 Updated EvtPythiaEngine to correctly handle updates of various particle    
  28ed8ea3 Implement most of the code review suggestions.

- Bc -> D0(star) mu nu, !279 (@gcorti) [LHCBGAUSS-1304]  
  Port merge of branch 'LHCBGAUSS-1304'  
  See merge request lhcb/Gauss!249 for Sim09  
  (cherry picked from commit d6500497be3bdd287162c995daa26e8d3e5ba7c1)  
  ff177130 Bc -> D0(star) mu nu    
  7503eba3 Modifications to the BcXMuNu/BCXFF classes.    
  af7b3796 Replace hardcoded particle masses with EvtPD::getMass().    
  c2afe32f Change EvtPDL::getMass() to EvtPDL::getMeanMass() in EvtBCXFF classes.


- Update generators version to use latest one in LCG_91, !236 (@gcorti)   
  Update version of rivet and yoda as well as thepeg to use the latest ones available in LCG_91.  
  The first two are for tuning, the second is to be used with Herwig++.


#### Fast Simulation

- Set the fromRedecay flag, !284 (@gcorti) [LHCBGAUSS-1357]  
  Port merge of branch 'LHCBGAUSS-1357'  
  See merge request lhcb/Gauss!270 into 'Sim09'  
  (cherry picked from commit 42b6fc2a721d3d443c0c7390865491d1cf711219)  
  f5601ea9 Set the fromRedecay flag



### Bug fixes

- LHCBGAUSS-1334.photonacceptance, !281 (@gcorti) [LHCBGAUSS-1334]  
  Port merge of branch 'LHCBGAUSS-1334.photonacceptance'  
  See merge request lhcb/Gauss!264 into 'Sim09'  
  (cherry picked from commit 5bc5b33f7b8db206d885a0a215dfc0404409d856)  
  e7f309a2 change from 4000mrad to 400mrad for photon acceptance as wanted

- LHCBGAUSS-876: Fixed Bug in RadLengthColl in GaussTools, !277 (@gcorti) [LHCBGAUSS-876]  
  Port merging of LHCBGAUSS-876.RadLengthCollFix  
  See merge request lhcb/Gauss!245 for Sim09  
  (cherry picked from commit ad8f10543d4d7bb987cfc2b5c62a82ea47e7c2b9)  
  3086f711 Fixed Bug in RadLengthColl where counters were not reset. Added option for easy…    
  50aaeb4a Fixed GaussGeo compatibility and added InterLength plots to VELO scan    
  a2104501 Make two test parts more consistent    
  88147f48 Update rad_length_scan.py to have correct header    
  6fca5ee5 Update rad_length_scan_velo_z.py to have correct header    
  ea9eba7c Added XY 2D Scans    
  0a6cf671 Set Batch Mode    
  a8fab663 Removed GiGaGeo option so test now runs with default reader in Gauss and added…    
  d6b72028 Updated Geo selection with fix    
  c3c04c4b Removed gROOT reset which breaks latest ROOT versions (gDirectory no longer…  

- LHCBGAUSS-1297.FixedMass, !252 (@philten) [LHCBGAUSS-1297]  
  Initial fix for fixed width issue.  
  This corresponds to !269 for 'Sim09' and !283 for 'Sim09-upgrade'

- Fixed pion duplication bug and replaced pion vertex check, !238 (@gcorti) [LHCBGAUSS-1272]  
  See merge request lhcb/Gauss!231 i.e. merge branch 'cherry-pick-c62ced01' into 'Sim09-upgrade'   
  (cherry picked from commit cb05535b5e68be737252b7e5f13b26509225ec0a)  
  589da13a Merge branch 'LHCBGAUSS-1272.DaughtersInLHCbAndCutsForDstar3pi' into 'Sim09'


### Code modernisations and cleanups

- Fix AmpGen CMake config to include TBB, !244 (@dpopov)   
  Fix for AmpGen's CMake configuration to include TBB to fix linking problem for `slc6-gcc62` in master (v52r0 built against Gaudi v29r0 and LHCb v43r1).


### Monitoring changes and Changes to tests

- LHCBGAUSS-869: Set Log Output to WARNING to greatly reduce log size and run time, !308 (@kzarebsk) [LHCBGAUSS-869]  
  Owing to the large file size of the log file generated and the significant reduction (~10 factor) in the run time of the test the output information has been set to WARNING.   
  The majority of output/run time is due to the listing of each event being produced of the 10000 per combination.  
  It is hoped this is the reason for the test currently being absent from LHCbPR listings despite running successfully.

- LHCBGAUSS-876: Fixed Incorrect Rich1 Plane2Plane, !296 (@kzarebsk) [LHCBGAUSS-876]  
  #### RICH1 Profile Fix  
  Due to the position of Scoring Plane 1 meaning a clash in Geant4 volumes the RadLength test must be run in two stages. It was recently discovered that the results for Rich1 Plane-to-Plane radiation length profiles were incorrect due to VELO components also being included. To fix this the test is now run in three stages:  
  - Full geometry for producing cumulative radiation and interaction length profiles  
  - VELO only for Plane-to-Plane profile for the VELO + PuVeto  
  - No VELO and no PuVeto for all other Plane-to-Plane profiles      
  #### Updated Profiles  
  - For better resolution of 2D scans, the method for firing the particle gun has been switched from random to being an eta-phi grid where the number of events is determined by nStepsEta*nStepsPhi.  
  - Option for adding a cap to Z maximum of the interaction/radiation length profiles for Rich1 and Velo has been added to reduce saturation due to contribution from the beam pipe.  
  #### Other  
  - `TNamed` added for each plot as a description to be picked up by LHCbPR.

- LHCBGAUSS-869: Fixed GaussGeo Issue in non-Sim09 versions, !263 (@kzarebsk) [LHCBGAUSS-869]  
  For all non-sim09 branches the selection of GaussGeo fails currently due to a mix up between using half GiGaGeo and half GaussGeo. As Sim09 works I suggest it be left alone and this fix only be ported to master (I think merging this into Sim09 would cause conflicts anyway).  
  **Has been tested with `lhcb-gauss-dev` and `lhcb-g4r103`**

- LHCBGAUSS-869: Added Energy Iterations and Materials to Titles (to master), !256 (@kzarebsk) [LHCBGAUSS-869]  
  The original design of the test did not state the material and particle names within the titles for each histogram. As this test forms part of LHCbPR it is important to be able to distinguish them.  
  Furthermore it was realised that despite a list of energies being provided the plots were not created for each energy, this has now been fixed by adding the loop statement in `Sim/SimChecks/options/MakeEvents.py`.  
  The changes have been tested using `lhcb-gauss-dev` and so are confirmed to work in `master`, `CMT`/`CMake`.  
  **A Seperate Similar Merge Request Has Been Made for `Sim09`, lhcb/Gauss!257**    
  This has been cherry-picked for 'Sim09-upgrade' in lhcb/Gauss!260

- Adds profile histograms and gives plots more meaningful titles, !253 (@gcorti) [LHCBGAUSS-874]  
  Porting to master the merge of LHCBGAUSS-874.profhists into 'Sim09'  
  See merge request lhcb/Gauss!247  
  (cherry picked from commit 9469bfc890ea5c7852646bed895f24de382ba5c6)  
  1f1c8d13 Adds profile histograms for more easier comparisons  
  e0b77e70 Names Titles more meaningfully

- Update configuration of the script needed to run the EM test., !240 (@gcorti) [LHCBGAUSS-1120]  
  Merge branch 'cherry-pick-b6f6f128' into 'Sim09-upgrade, i.e. port merge request lhcb/Gauss!233  
  that was (cherry picked from commit 872e1975479d57db45e7285aa5366fdfbe246ece), i.e.   
  4f53caf1 Merge branch 'LHCBGAUSS-1120.Update' into 'Sim09', i.e. MR lhcb/Gauss!222

- New reference for nightly tests, !312 (@gcorti)   


  
