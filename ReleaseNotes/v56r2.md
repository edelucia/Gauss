2022-11-21 Gauss v56r2
===

This version uses 
Geant4 [v10r6p2t6](../../../../Geant4/-/tags/v10r6p2t6),
Run2Support [v2r2p2](../../../../Run2Support/-/tags/v2r2p2),
LHCb [v53r9p3](../../../../LHCb/-/tags/v53r9p3),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5),
Detector [v1r2](../../../../Detector/-/tags/v1r2) and
LCG [101_LHCB_7](http://lcginfo.cern.ch/release/101_LHCB_7/) with ROOT 6.24.06.

The following generators are used from LCG [101_LHCB_7](http://lcginfo.cern.ch/release/101_LHCB_7/) with ROOT 6.24.06:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- madgraph  2.9.3.atlas1
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, starlight r313
- rivet     3.1.4,     yoda   1.9.0   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v56r1](../-/tags/v56r1), with the following changes:

### New features ~"new feature"

- ~Configuration | Add 2022/2023 datatypes to Gauss, !906 (@adavis)
- ~Geant4 | Add new Geant4 SUSY particles, !892 (@melashri)
- ~Generators ~Decays | Addition of D02pipipi0_v1 model to LbAmpGen (LHCBGAUSS-2660), !890 (@lali) [LHCBGAUSS-2660]


### Fixes ~"bug fix" ~workaround

- ~Simulation ~Geant4 | Reject specific particles in MCCollector::SensDet, !895 (@mimazure) [#70]
- ~Generators | Fix cross section option for pythia8.2, !904 (@sbashir)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Update references for Gauss v56r2, !908 (@gcorti)


