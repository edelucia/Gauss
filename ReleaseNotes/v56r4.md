2023-04-25 Gauss v56r4
===

This version uses 
Geant4 [v10r6p2t7](../../../../Geant4/-/tags/v10r6p2t7),
Run2Support [v2r2p3](../../../../Run2Support/-/tags/v2r2p3),
LHCb [v53r9p4](../../../../LHCb/-/tags/v53r9p4),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [102b_LHCB_7](http://lcginfo.cern.ch/release/102b_LHCB_7/) with ROOT 6.26.08.

The following generators are used from LCG [102b_LHCB_7](http://lcginfo.cern.ch/release/102b_LHCB_7/) with ROOT 6.26.08:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- madgraph  2.9.3.atlas1
- herwig++  7.2.1,      thepeg 2.2.1
- crmc      1.8.0.lhcb, starlight r313
- rivet     3.1.4,     yoda   1.9.0   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB
- Hijing (1.383bs.2)


This version is released on `master` branch.
Built relative to Gauss [v56r3](../-/tags/v56r3), with the following changes:


### Enhancements ~enhancement

- ~Generators | Add GenXicc support for sqrt(s)=13.6TeV, !964 (@kreps)
- ~Generators | Add BcVegPy support for sqrt(s)=13.6TeV, !962 (@kreps)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Generators | Adding PileUpTool to Madgraph adaptor, !960 (@acasaisv)
- ~Build | Use reference in for loops in ConversionFilter and MonitorTiming, !963 (@kreps)


