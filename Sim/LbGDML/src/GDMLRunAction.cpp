/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/// Tool factory 
/// Local 
#include "GDMLRunAction.h"

/// GDML writer
#include "G4VPhysicalVolume.hh"
#include "G4TransportationManager.hh"
#include "G4GDMLParser.hh"

DECLARE_COMPONENT( GDMLRunAction )

// ============================================================================
/** standard constructor 
 *  @see GiGaPhysListBase
 *  @see GiGaBase 
 *  @see AlgTool 
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================
GDMLRunAction::GDMLRunAction(const std::string& type,
                             const std::string& name,
                             const IInterface*  parent) : 
    GiGaRunActionBase(type, name, parent),
    m_done(false) {  

  declareProperty("Schema", m_schema = "$GDML_base/src/GDMLSchema/gdml.xsd");
  declareProperty("Output", m_outfile = "LHCb.gdml");
  declareProperty("AddReferences", m_refs = true);

}

// ============================================================================
/// Action performed at the begin of each run
// ============================================================================
void GDMLRunAction::BeginOfRunAction(const G4Run* run) {

  if (0 == run) { 
    Warning("BeginOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); 
  }

  if (m_done) return;
  m_done = true;
  /// Get world volume.
  auto transMgr = G4TransportationManager::GetTransportationManager();
  auto g4wv = transMgr->GetNavigatorForTracking()->GetWorldVolume();
  bool materialNull = false;
  if( !m_parallelWorld.value().empty() ) {
    auto mass_material = g4wv->GetLogicalVolume()->GetMaterial();
    g4wv = transMgr->GetParallelWorld( m_parallelWorld.value() );
    if (!g4wv->GetLogicalVolume()->GetMaterial()) {
      warning() << "Parallel world's material is nullptr! "
                << "This is ok for simulation, but not for GDML export. "
                << "Setting it temporarily to mass world's material" << endmsg;
      g4wv->GetLogicalVolume()->SetMaterial(mass_material);
      materialNull = true;
    }
  }
  if (!g4wv) {
    error() << "Null pointer to world volume" << endmsg;
    return;
  } 
  G4GDMLParser g4writer;
  // export auxilliary information
  g4writer.SetSDExport(m_exportSD.value());
  g4writer.SetEnergyCutsExport(m_exportEnergyCuts.value());
  g4writer.SetRegionExport(m_exportRegion.value());

  try {
    g4writer.Write(m_outfile, g4wv, m_refs, m_schema);
  } catch(std::logic_error &lerr) {
    error() << "Caught an exception " << lerr.what() << endmsg;
  }

  if(materialNull) {
    warning() << "Setting parallel world's material back to nullptr." << endmsg;
    g4wv->GetLogicalVolume()->SetMaterial(nullptr);
  }

}

