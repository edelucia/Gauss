###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions('$APPCONFIGOPTS/Gauss/Test-pAr-Beam6500GeV-0GeV-md100-2015-fix1.py')
importOptions('$GAUSSOPTS/BeamGas.py')
importOptions('$GAUSSOPTS/DBTags-2015.py')
importOptions('$APPCONFIGOPTS/Gauss/DataType-2015.py')

from Configurables import Gauss
from GaudiKernel import SystemOfUnits
Gauss().FixedTargetParticle = 'Ar'
Gauss().FixedTargetLuminosity = 1*(10**26)/(SystemOfUnits.cm2*SystemOfUnits.s)
Gauss().FixedTargetXSection = 620.*SystemOfUnits.millibarn
