/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SimInit.h,v 1.3 2009-03-26 21:32:40 robbep Exp $
#ifndef SIMINIT_H
#define SIMINIT_H 1

// Include files
// from LHCbKernel
#include "Kernel/LbAppInit.h"

// from GenEvent
#include "Event/GenHeader.h"

// from MCEvent
#include "Event/MCHeader.h"


class IGenericTool;
class IDetDataSvc;

/** @class SimInit SimInit.h
 *
 *  First TopAlg Simulation phase of Gauss.
 *  Initializes random number and fills memory histogram.
 *  It also creates and fills the MCHeader.
 *
 *  @author Gloria CORTI
 *  @date   2006-01-16
 */
class SimInit : public LbAppInit {
public:
  /// Standard constructor
  using LbAppInit::LbAppInit;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  IGenericTool* m_memoryTool{nullptr};   ///< Pointer to (private) memory histogram tool
  IDetDataSvc*  m_detDataSvc{nullptr};   ///< Pointer to (private) detector service

  Gaudi::Property<std::string> m_genHeader{this,"GenHeader",LHCb::GenHeaderLocation::Default,"Location where to read Gen Header"} ; ///< Location where to read Gen Header
  Gaudi::Property<std::string> m_mcHeader{this,"MCHeader",LHCb::MCHeaderLocation::Default ,"Location where to store MC Header"} ; ///< Location where to store MC Header

};
#endif // SIMINIT_H
