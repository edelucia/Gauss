###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import * 
from Configurables import Gauss 
import os, sys

def configureGenerator ( generator_type, event_type ): 
    """Helper function to configure the generator for Lamarr

    Arguments:
      generator_type - string
        Define the type of generator to be used, can be: "ParticleGun" or "Pythia". 

      event_type - string 
        event type to configure EvtGen 

    """

    importOptions('$DECFILESOPTS/%s.py'%event_type)

    if generator_type.capitalize() in ['Pgun','Particlegun']:
        importOptions("$LBPGUNSROOT/options/PGuns.py")
        from Configurables import ToolSvc
        from Configurables import EvtGenDecay
        from Configurables import ParticleGun

        # if the dec file already has a particle gun configuration, 
        # pass it here, else, configure a flat momentum spectrum

        if hasattr(ParticleGun(),'SignalPdgCode'):
            print ('ParticleGun configuration already set')
            #no configuration necessary
            pass
        elif hasattr(ParticleGun(),'MomentumRange'):
            if hasattr(ParticleGun().MomentumRange,"PdgCodes"):
                print ( 'ParticleGun Momentum range: ' +
                          str(ParticleGun().MomentumRange)  ) 
                pass
            else:
                print ('problem with configuration!')
                import sys
                sys.exit()
        else:
            print ('WARNING! ParticleGun momentum range not set! Using default!!!')
            from Configurables import MomentumRange
            ParticleGun().addTool( MomentumRange )
            from GaudiKernel import SystemOfUnits
            ParticleGun().MomentumRange.MomentumMin = 1.0*SystemOfUnits.GeV
            from GaudiKernel import SystemOfUnits
            ParticleGun().MomentumRange.MomentumMax = 100.*SystemOfUnits.GeV
            ParticleGun().EventType = event_type
                    
            ParticleGun().ParticleGunTool = "MomentumRange"
            ParticleGun().NumberOfParticlesTool = "FlatNParticles"
            #figure this out
            from Configurables import Generation
            pid_list = []
            if hasattr(Generation(),'SignalRepeatedHadronization'):
                pid_list = Generation().SignalRepeatedHadronization.SignalPIDList
            elif hasattr(Generation(),"SignalPlain"):
                pid_list = Generation().SignalPlain.SignalPIDList
            else:
                print ('major configuration problem, please fix!')
                import sys
                sys.exit()

            print( 'ParticleGun will use signal PID list: ',pid_list)
            ParticleGun().MomentumRange.PdgCodes = pid_list
            ParticleGun().SignalPdgCode = abs(pid_list[0])
            ParticleGun().DecayTool="EvtGenDecay"
        
    elif generator_type in ['Pythia','Pythia8']:
        print ("Configuring Pythia8") 
        importOptions('$DECFILESOPTS/%s.py'%event_type)
        importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
        #fix pythia 8 color reconnection problem
        importOptions("$APPCONFIGOPTS/Gauss/TuningPythia8_Sim09.py")

    else:
      raise NotImplementedError ( "Failed configuration for GeneratorType %s" % 
          generator_type )
