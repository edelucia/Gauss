################################################################################ 
#  (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                              #
#  This software is distributed under the terms of the GNU General Public      #
#  Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                              #
#  In applying this licence, CERN does not waive the privileges and immunities #
#  granted to it by virtue of its status as an Intergovernmental Organization  #
#  or submit itself to any jurisdiction.                                       #
################################################################################
"""
read_profile.py 

This simple script loads the JSON files produced with SimpleProfiler and
displays a table with the timing of each algorithm.

The format of the output table is as follows:

Name-of-the-algorithm  |  total-time  |  max-time/average-time  |  num-of-calls

"""
from argparse import ArgumentParser
import json
import numpy as np 
import os.path

# --------------------------------------------------------------------------------
# insert

def insert (tree, key, value):
  """
  Inernal. Tree traversal to insert new nodes
  """
  if not isinstance(key, (tuple, list)):
    key = [key]

  if len(key) == 0:
    return 

  if key[0] not in tree:
    tree[key[0]] = dict(value=None, children={})

  if len(key) == 1:
    tree[key[0]]['value'] = value
  else:
    insert(tree[key[0]]['children'], key[1:], value)


# ------------------------------------------------------------------------------  
# dump

def dump(tree, indent=0):
  """
  Internal. Tree traversal to print out the table 
  """
  ls = [(k, v['value']) for k, v in tree.items()]
  for i in  np.argsort([v.sum() if v is not None else 0 for _, v in ls]):
    k, v = ls[i]
    if v is None:
      print ("  "*indent + f"{k} {'='*(120 - len(k))}")
    else:
      print ("  "*indent + 
          f"{k:30s} {'  '*(5-indent)} {v.sum():10.2f} s   "
          f"{v.max()/v.mean():10.2f} {len(v):10.0f}"
          )

    dump (tree[k]['children'], indent=indent+1)
    

# ------------------------------------------------------------------------------  
# analyse

def analyse (data):
  """
  Analysis of the loaded data.
  """
  tree = {}
  for algoname, profile in data.items():
    for k, v in profile.items():
      v = np.array(v, dtype=np.float64)
      insert (
          tree=tree,
          key=[algoname] + k.split("\\"),
          value=v #
      )

  dump(tree)

# ------------------------------------------------------------------------------  
# main

def main():
  """
  Parse command line arguments and JSON data, then calls analyse()
  """
  parser = ArgumentParser()
  parser.add_argument ("files", nargs='+', 
    help="JSON file as produced by SimpleProfiler")
  args = parser.parse_args()

  data = dict()
  for f in args.files:
    name = os.path.basename(f)
    name = name[:name.index('.')]
    data[name] = json.load(open(f))

  analyse (data)


################################################################################
## Entry point
if __name__ == '__main__':
  main()


