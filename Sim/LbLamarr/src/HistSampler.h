/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _LBLAMARR_HISTSAMPLER_H_
#define _LBLAMARR_HISTSAMPLER_H_ 

#include <string>
#include <vector> 

class HistSampler 
{
  public:
    HistSampler ( const std::string& file_name, 
                  const std::string& hist_name, 
                  size_t nSamples = 100 );

    float sample ( const float xVar, const float rndmFlat ) const; 



  private:
    std::vector <float> m_xAxis;
    std::vector < std::vector <float> > m_yAxis; 



  
  public: // Exceptions   
    class FileNotFoundException: public std::exception
    {
      public: 
        FileNotFoundException ( const std::string& f ): std::exception(), m_fname (f) {}

        virtual const char* what() const throw() override 
        {
          return "File cannot be open. ";
        }
      private:
        std::string m_fname; 
    };

    class HistNotFoundException: public std::exception
    {
      public: 
        HistNotFoundException ( const std::string& h ): std::exception(), m_hname (h) {}

        virtual const char* what() const throw() override 
        {
          return "Histogram cannot be found."; 
        }
      private:
        std::string m_hname; 
    };



};

#endif 

