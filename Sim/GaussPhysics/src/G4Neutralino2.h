/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef G4Neutralino2_h
#define G4Neutralino2_h 1 

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

/** @class  G4Neutralino2 G4Neutralino2.h
 *
 *  Define the Neutralino2 in Geant
 *
 *  @author Neal Gueissaz
 *  @date   2008-09-12
 */

// ######################################################################
// ###                         Neutralino2                             ###
// ######################################################################

class G4Neutralino2 : public G4ParticleDefinition
{
 private:
  static G4Neutralino2 * theInstance ;
  G4Neutralino2( ) { }
  ~G4Neutralino2( ) { }

 public:
   static G4Neutralino2* Definition();
   static G4Neutralino2* Neutralino2Definition();
   static G4Neutralino2* Neutralino2();
};

#endif

