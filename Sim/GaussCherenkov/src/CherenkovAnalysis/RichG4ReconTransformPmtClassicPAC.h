/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RICHANALYSIS_RICHG4RECONTRANSFORMPMTCLASSICPAC_H
#define RICHANALYSIS_RICHG4RECONTRANSFORMPMTCLASSICPAC_H 1
// Include files
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ILVolume.h"
#include "DetDesc/IPVolume.h"
#include "DetDesc/Material.h"
#include "GaudiKernel/Transform3DTypes.h"
 

/** @class RichG4ReconTransformPmtClassicPAC RichG4ReconTransformPmtClassicPAC.h RichAnalysis/RichG4ReconTransformPmtClassicPAC.h
 *
 *
 *
 *  @author Sajan EASO
 *  @date   2003-09-09
 */
class RichG4ReconTransformPmtClassicPAC {

public:

  /// Standard constructor
  RichG4ReconTransformPmtClassicPAC( );

  RichG4ReconTransformPmtClassicPAC(int aRichDetNum, int PmtModuleNum, int aPmtNum );

  virtual ~RichG4ReconTransformPmtClassicPAC( ); ///< Destructor

  const Gaudi::Transform3D & PmtLocalToGlobal()
  {
    return m_PmtLocalToGlobal;
  }

  const Gaudi::Transform3D & PmtGlobalToLocal()
  {
    return m_PmtGlobalToLocal;
  }
  const Gaudi::Transform3D & PmtPhDetPanelToLocal() 
  {  return m_PmtPhDetPanelToLocal;}
  const Gaudi::Transform3D & PmtLocalToPmtPhDetPanel()
  {   return m_PmtLocalToPmtPhDetPanel;}
  
    
  
  void initialise();
protected:

private:

  Gaudi::Transform3D m_PmtLocalToGlobal;

  Gaudi::Transform3D m_PmtGlobalToLocal;


  Gaudi::Transform3D m_PmtPhDetPanelToLocal;
  Gaudi::Transform3D m_PmtLocalToPmtPhDetPanel;
  
  int   m_Rich1SubMasterPvIndex;

  std::string   m_Rich1MagShPvName0;
  std::string   m_Rich1MagShPvName1;
  int   m_Rich1PhotDetSupPvIndex;
  int  m_RichPmtInModuleStartIndex;
  
  int   m_PmtSMasterIndex;
  // int   m_Rich2PmtPanelIndex0;
  // int   m_Rich2PmtPanelIndex1;

  int m_Rich1PmtArrayMaxH0;
  int m_Rich2PmtArrayMaxH0;

  int m_Rich1PmtModuleMaxH0;
  int m_Rich2PmtModuleMaxH0;
  int m_RichNumPmtInModule;

  //  int m_Rich2N2EnclIndex0;
  // int m_Rich2N2EnclIndex1;

  std::string m_Rich2PmtPanelName0;
  std::string  m_Rich2PmtPanelName1;
  std::string m_Rich2PmtN2EnclName0;
  std::string m_Rich2PmtN2EnclName1;
  std::string m_Rich1PmtModuleMasterNamePrefClassic;
  std::string m_Rich1PmtModuleMasterNamePrefLens;
  std::vector<std::string> m_Rich1PmtModuleMasterNameSuf;

  std::string m_Rich1PhtDetSupName0;
  std::string m_Rich1PhtDetSupName1;
  

  //  int  m_Rich1MagShPvIndexH0;
  // int   m_Rich1MagShPvIndexH1;

};
#endif // RICHANALYSIS_RICHG4RECONTRANSFORMPMTCLASSICPAC_H
