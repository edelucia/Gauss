/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CkvG4RunAction.h,v 1.9 2007-01-12 15:32:04 ranjard Exp $
#ifndef CkvG4RunAction_h
#define CkvG4RunAction_h 1

// Include files
// GiGa
#include "GiGa/GiGaRunActionBase.h"

// forward declarations
class RichG4HistoDefineSet1;
class CherenkovG4HistoDefineSet2;
class RichG4HistoDefineSet3;
class CherenkovG4HistoDefineSet4;
class CherenkovG4HistoDefineSet5;
class RichG4HistoDefineTimer;

/** @class GiGaRunActionBase GiGaRunActionBase.h src/RichActions//GiGaRunActionBase.h
 *
 *  @author Sajan Easo
 *  @author Gloria Corti (port to Gaudi v19)
 *  @date   2002-08-21, last modified 2007-01-11
 *  @ date 2011-03-08
 */

class CkvG4RunAction: public virtual GiGaRunActionBase
{
public:

  /// useful typedef
  typedef  std::vector<std::string> COMMANDS;

  /** performe the action at the begin of each run
   *  @param run pointer to Geant4 run object
   */
  void BeginOfRunAction ( const G4Run* run ) override;

  /** performe the action at the end  of each event
   *  @param run pointer to Geant4 run object
   */
  void EndOfRunAction   ( const G4Run* run ) override;


  /** standard constructor
   *  @see GiGaPhysListBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  CkvG4RunAction
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  
  RichG4HistoDefineSet1* aRichG4HistoDefineSet1()
  {return  m_aRichG4HistoSet1; }

  CherenkovG4HistoDefineSet2* aRichG4HistoDefineSet2()
  {return  m_aRichG4HistoSet2; }

  RichG4HistoDefineSet3* aRichG4HistoDefineSet3()
  {return  m_aRichG4HistoSet3; }


  bool defineRichG4HistoSet1()
  {
    return  m_defineRichG4HistoSet1.value();

  }
  void setdefineRichG4HistoSet1(bool aboolValue)
  {
    m_defineRichG4HistoSet1.value()= aboolValue;
  }
  bool defineRichG4HistoSet2()
  {
    return  m_defineRichG4HistoSet2.value();

  }
  void setdefineRichG4HistoSet2(bool aboolValue2)
  {
    m_defineRichG4HistoSet2.value()= aboolValue2;
  }
  void setdefineRichG4HistoSet3(bool aboolValue3)
  {
    m_defineRichG4HistoSet3.value()= aboolValue3;
  }
  void setdefineRichG4HistoSet4(bool aboolValue4)
  {
    m_defineRichG4HistoSet4.value()= aboolValue4;
  }
  void setdefineRichG4HistoSet5(bool aboolValue5)
  {
    m_defineRichG4HistoSet5.value()= aboolValue5;
  }

  bool FirstTimeOfBeginRichRun() {
    return m_FirstTimeOfBeginRichRun;}

  void setFirstTimeOfBeginRichRun( bool aboolValue) {
    m_FirstTimeOfBeginRichRun=aboolValue;}


private:

  ///no default constructor
  CkvG4RunAction();
  /// no copy constructor
  CkvG4RunAction( const CkvG4RunAction& );
  /// no assignement
  CkvG4RunAction& operator=( const CkvG4RunAction& );

private:

  Gaudi::Property<COMMANDS>  m_beginCmds{this,"BeginOfRunCommands",{},"BeginOfRunCommands"};
  Gaudi::Property<COMMANDS>  m_endCmds{this,"EndOfRunCommands",{},"EndOfRunCommands"};
  Gaudi::Property<bool> m_defineRichG4HistoSet1{this,"DefineRichG4HistoSet1",false,"DefineRichG4HistoSet1"};
  Gaudi::Property<bool> m_defineRichG4HistoSet2{this,"DefineRichG4HistoSet2",false,"DefineRichG4HistoSet2"};
  Gaudi::Property<bool> m_defineRichG4HistoSet3{this,"DefineRichG4HistoSet3",false,"DefineRichG4HistoSet3"};
  Gaudi::Property<bool> m_defineRichG4HistoSet4{this,"DefineRichG4HistoSet4",false,"DefineRichG4HistoSet4"};
  Gaudi::Property<bool> m_defineRichG4HistoSet5{this,"DefineRichG4HistoSet5",false,"DefineRichG4HistoSet5"};
  Gaudi::Property<bool> m_defineRichG4HistoTimer{this,"DefineRichG4HistoTimer",false,"DefineRichG4HistoTimer"};


  RichG4HistoDefineSet1* m_aRichG4HistoSet1{nullptr};
  CherenkovG4HistoDefineSet2* m_aRichG4HistoSet2{nullptr};
  RichG4HistoDefineSet3* m_aRichG4HistoSet3{nullptr};
  CherenkovG4HistoDefineSet4* m_aRichG4HistoSet4{nullptr};
  CherenkovG4HistoDefineSet5* m_aRichG4HistoSet5{nullptr};
  RichG4HistoDefineTimer* m_aRichG4HistoTimer{nullptr};

  bool  m_FirstTimeOfBeginRichRun{true};

};

#endif ///< GIGA_GIGARUNACTIONCOMMAND_H

