/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef TORCHTESTBEAMANALYSIS_TORCHTBBASICSTEPANALYSIS_H
#define TORCHTESTBEAMANALYSIS_TORCHTBBASICSTEPANALYSIS_H 1

// Include files
#include "GiGa/GiGaStepActionBase.h"
template <class TYPE> class GiGaFactory;
class G4Step;


/** @class TorchTBBasicStepAnalysis TorchTBBasicStepAnalysis.h TorchTestBeamAnalysis/TorchTBBasicStepAnalysis.h
 *
 *
 *  @author Sajan Easo
 *  @date   2012-05-30
 */
class TorchTBBasicStepAnalysis:virtual public  GiGaStepActionBase {
  friend class GiGaFactory<TorchTBBasicStepAnalysis>;

public:
  /// Standard constructor
  TorchTBBasicStepAnalysis(const std::string& type   ,
                           const std::string& name   ,
                          const IInterface*  parent  );

  ~TorchTBBasicStepAnalysis( ); ///< Destructor
  void UserSteppingAction( const G4Step* aStep ) override;

protected:

private:
  TorchTBBasicStepAnalysis();
  TorchTBBasicStepAnalysis(const TorchTBBasicStepAnalysis&);
  TorchTBBasicStepAnalysis& operator=(const TorchTBBasicStepAnalysis& );

};
#endif // TORCHTESTBEAMANALYSIS_TORCHTBBASICSTEPANALYSIS_H
