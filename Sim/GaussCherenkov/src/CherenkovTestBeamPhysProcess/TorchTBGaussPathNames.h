/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHERENKOVTESTBEAMPHYSPROCESS_TORCHTBGAUSSPATHNAMES_H
#define CHERENKOVTESTBEAMPHYSPROCESS_TORCHTBGAUSSPATHNAMES_H 1

#include "globals.hh"


static const G4String TorchTBMcpQeffMatTabPropPath="/dd/Materials/TorchTestBeamMaterialTabProperties/NominalMCPQuantumEff";

static const G4String TorchTBMcpHVMatTabPropPath="/dd/Materials/TorchTestBeamMaterialTabProperties/McpHighVoltage";


static const G4String TorchMcpQuartzWindowMaterialName="/dd/Materials/TorchMaterials/TorchMCPWindowMateral";
static const G4String TorchTBQuartzBarMaterialName="/dd/Materials/TorchTestBeamMaterials/TorchTestBeamBorosilicate";

static const G4String TorchMcpPhCathMatName=
                            "/dd/Materials/TorchMaterials/TorchMCPPhCathodeMaterial" ;

static const G4String TorchMcpVacName=
                       "/dd/Materials/TorchMaterials/TorchMCPVacuum";

static const G4String TorchTBMcpQwDeSubPathName ="/TorchFirsMcpQW";
static const G4String TorchTBMcpPhCathDeSubPathname="/TorchFirstMcpPhCathode";
static const G4String TorchTBMcpAnodeSensDetNamePrefix="TorchAnodeSensDet";

static const G4String TorchTBDeStructurePathName = "/dd/Structure/LHCb/AfterMagnetRegion/TorchTBMasterDet";



#endif // CHERENKOVTESTBEAMPHYSPROCESS_TORCHTBGAUSSPATHNAMES_H
