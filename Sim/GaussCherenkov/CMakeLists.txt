###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussCherenkov
------------------
#]=======================================================================]

gaudi_add_module(GaussCherenkov
    SOURCES
        src/Factories.cpp
        src/SensDet/CkvCommonSensDet.cpp
        src/SensDet/CkvG4GeomProp.cpp
        src/SensDet/CkvG4Hit.cpp
        src/SensDet/CkvGrandSensDet.cpp
        src/SensDet/CkvSensDet.cpp
        src/SensDet/CkvStdSensDet.cpp
        src/SensDet/GetMCCkvHitsAlg.cpp
        src/SensDet/GetMCCkvInfoBase.cpp
        src/SensDet/GetMCCkvOpticalPhotonsAlg.cpp
        src/SensDet/GetMCCkvSegmentsAlg.cpp
        src/SensDet/GetMCCkvTracksAlg.cpp
        src/CherenkovPhysProcess/GiGaPhysConstructorOpCkv.cpp
        src/CherenkovPhysProcess/GiGaPhysConstructorPhotoDetector.cpp
        src/CkvPhysPhotDet/RichPmtPSF.cpp
        src/CkvPhysPhotDet/RichPmtPhotoElectricEffect.cpp
        src/CkvPhysPhotDet/RichPmtProperties.cpp
        src/CkvPhysPhotDet/RichPmtQE.cpp
        src/CkvPhysPhotDet/RichPmtSiEnergyLoss.cpp
        src/srcG4Ckv/CkvG4OpBoundaryProcess.cc
        src/CherenkovAction/CherenkovG4EventAction.cpp
        src/CherenkovAction/CkvG4RunAction.cpp
        src/CherenkovAction/CkvG4TrackActionPhotOpt.cpp
        src/CherenkovAnalysis/CherenkovG4CkvRecon.cpp
        src/CherenkovAnalysis/CherenkovG4Counters.cpp
        src/CherenkovAnalysis/CherenkovG4HistoDefineSet2.cpp
        src/CherenkovAnalysis/CherenkovG4HistoDefineSet4.cpp
        src/CherenkovAnalysis/CherenkovG4HistoDefineSet5.cpp
        src/CherenkovAnalysis/CherenkovG4HistoDefineSet8.cpp
        src/CherenkovAnalysis/CherenkovG4HistoFillSet2.cpp
        src/CherenkovAnalysis/CherenkovG4HistoFillSet4.cpp
        src/CherenkovAnalysis/CherenkovG4HistoFillSet5.cpp
        src/CherenkovAnalysis/CherenkovG4HistoHitTime.cpp
        src/CherenkovAnalysis/CherenkovG4HitRecon.cpp
        src/CherenkovAnalysis/CherenkovG4StepAnalysis6.cpp
        src/CherenkovAnalysis/CherenkovStepAnalysis7.cpp
        src/CherenkovAnalysis/CherenkovStepAnalysis8.cpp
        src/CherenkovAnalysis/CkvG4EventHitCount.cpp
        src/CherenkovAnalysis/CkvG4HistoFillSet1.cpp
        src/CherenkovAnalysis/CkvG4ReconFlatMirr.cpp
        src/CherenkovAnalysis/CkvG4StepAnalysis11.cpp
        src/CherenkovAnalysis/CkvG4StepAnalysis5.cpp
        src/CherenkovAnalysis/CkvG4StepAnalysis9.cpp
        src/CherenkovAnalysis/RichG4ReconPmt.cpp
        src/CherenkovAnalysis/RichG4ReconTransformPmt.cpp
        src/CherenkovAnalysis/RichG4ReconTransformPmtClassicPAC.cpp
        src/CherenkovMisc/CherenkovG4PmtReflTag.cpp
        src/CherenkovMisc/CherenkovPmtLensUtil.cpp
        src/CherenkovMisc/CkvG4HitCollectionName.cpp
        src/CherenkovMisc/CkvG4SvcLocator.cpp
        src/CherenkovMisc/CkvGeometrySetupUtil.cpp
        src/CherenkovTestBeamPhysProcess/TorchTBMcpEnergyLoss.cpp
        src/CherenkovTestBeamPhysProcess/TorchTBMcpPhotoElectricEffect.cpp
        src/CherenkovTestBeamPhysProcess/TorchTBMcpProperties.cpp
        src/CherenkovTestBeamSensDet/TorchTBHitCollName.cpp
        src/CherenkovTestBeamSensDet/TorchTBMcpSensDet.cpp
        src/TorchTestBeamAnalysis/TorchTBBasicStepAnalysis.cpp
        src/TorchTestBeamAnalysis/TorchTBEventAction.cpp
        src/TorchTestBeamAnalysis/TorchTBG4DefineHistSet6.cpp
        src/TorchTestBeamAnalysis/TorchTBG4FillHistoSet6.cpp
        src/TorchTestBeamAnalysis/TorchTBHitAnalysis.cpp
        src/TorchTestBeamAnalysis/TorchTBRunAction.cpp
    LINK
        Gauss::GaussRICHLib
    GENCONF_OPTIONS
        --load-library=GaussToolsGenConfHelperLib
)
add_dependencies(GaussCherenkov Gauss::GaussToolsGenConfHelperLib)

# FIXME: this should be fixed, cross-referencing header files!
target_include_directories(GaussCherenkov PRIVATE 
    ${CMAKE_CURRENT_SOURCE_DIR}/src/include
    ${CMAKE_CURRENT_SOURCE_DIR}/src/SensDet
    ${CMAKE_CURRENT_SOURCE_DIR}/src/CkvPhysPhotDet
    ${CMAKE_CURRENT_SOURCE_DIR}/src/CherenkovAnalysis
    ${CMAKE_CURRENT_SOURCE_DIR}/src/CherenkovTestBeamPhysProcess
    ${CMAKE_CURRENT_SOURCE_DIR}/src/CherenkovTestBeamSensDet
)

gaudi_install(PYTHON)

gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    :GaussCherenkov
    Sim/GiGa:GiGa
)
