/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef    GIGA_IIDIGiGaPhysConstructor_H
#define    GIGA_IIDIGiGaPhysConstructor_H 1 
// ============================================================================

/** @file 
 *  declaration of the unique interface identifier for class IGiGaPhysConstructor
 *  @author Witek Pokorski Witold.Pokorski@cern.ch
 *  @date   30 August 2002
 */
/** @var IID_IGiGaPhysConstructor
 *  declaration of the unique interface identifier for class IGiGaPhysConstructor
 *  @author Witek Pokorski Witold.Pokorski@cern.ch
 *  @date   30 August 2002
 */
static const InterfaceID IID_IGiGaPhysConstructor( "IGiGaPhysConstructor",1,0);

// ============================================================================
// The End 
// ============================================================================
#endif  ///< GIGA_IIDIGiGaPhysConstructor_H
// ============================================================================
