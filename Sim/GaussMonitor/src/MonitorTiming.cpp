/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// CLHEP
#include "CLHEP/Geometry/Point3D.h"
// Geant4
#include "G4Electron.hh"
#include "G4Gamma.hh"
#include "G4OpticalPhoton.hh"
#include "G4ParticleTable.hh"
#include "G4Positron.hh"
#include "G4Step.hh"
#include "G4Timer.hh"
// Guadi
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/Stat.h"
// GiGa
#include "GiGa/IGiGaSvc.h"
// GaussTools
#include "GaussTools/GaussTrackInformation.h"
// AIDA
#include "AIDA/IHistogram1D.h"
// local
#include "MonitorTiming.h"
// STL
#include <fstream>

/* file
 *
 *  Implementation of class MonitorTiming for timing studies
 *
 *  author Olga Levitskaya, Gloria Corti, James McCarthy
 */

// ============================================================================
// Declaration of the Tool Factory
// ============================================================================
DECLARE_COMPONENT( MonitorTiming )

// ============================================================================
// Standard constructor, initialize variables
// ============================================================================
/** standard constructor
 *  @see GiGaStepActionBase
 *  @see GiGaBase
 *  @see AlgTool
 *  @param type type of the object (?)
 *  @param name name of the object
 *  @param parent  pointer to parent object
 */
// ============================================================================

MonitorTiming::MonitorTiming( std::string const& type, std::string const& name, IInterface const* parent )
    : GiGaStepActionBase( type, name, parent ) {
  // timer instantiation
  totalTimer = new G4Timer;
  stepTimer  = new G4Timer;

  steppingManager = new G4SteppingManager;
}

// ============================================================================
//  Destructor
// ============================================================================
MonitorTiming::~MonitorTiming() {
  delete totalTimer;
  delete stepTimer;
  delete steppingManager;
}

// =============================================================================
//  Initialization
// =============================================================================
StatusCode MonitorTiming::initialize() {

  info() << "*********** Initialising MonitorTimingTool ************" << endmsg;

  // initialise the total cumulated time
  totalCumTime = 0;
  totalTimer->Start();

  // Add 'All' category to the list if not specified explicitly
  if ( std::find( knownVolumes.value().begin(), knownVolumes.value().end(), detAll ) == knownVolumes.value().end() ) {
    knownVolumes.value().push_back( detAll );
  }

  // Check dets in groups
  for ( auto const& el : detsGroups.value() ) {
    if ( std::find( knownVolumes.value().begin(), knownVolumes.value().end(), el.first ) == knownVolumes.value().end() ) {
      detsGroups.value().erase( el.first );
      warning() << el.first << " is specified in detectors grouping, but is not a known volume." << endmsg;
    }
  }

  return Print( "Initialized successfully", GiGaStepActionBase::initialize(), MSG::VERBOSE );
}

StatusCode MonitorTiming::finalize() {
  info() << "*********** Finalising MonitorTimingTool ************" << endmsg;

  stepTimer->Stop();
  totalTimer->Stop();

  std::ofstream outf( m_TimerFileName.value() );
  if ( !outf.is_open() ) {
    info() << "MonitorTiming:: Timer File not opended :: EXIT" << endmsg;
    return StatusCode::FAILURE;
  }

  debug() << "************** Volume Timing **************" << endmsg;
  outf << "************** Volume Timing **************" << std::endl;
  {
    totalCumTime = 0.;
    int vol_indx = 0;
    for ( auto const& vol : sortMapByValue( allVolsTimes ) ) {
      totalCumTime += vol.first;
      outf << "Volume " << vol_indx << ": " << vol.second << " cumulated time " << vol.first << " seconds" << std::endl;
      vol_indx++;
    }
    outf << "Total Time: " << totalCumTime << std::endl;
  }

  debug() << "************** Other Volumes **************" << endmsg;
  outf << std::endl << std::endl << "************** Other Volumes **************" << std::endl;
  for ( size_t i = 0; i < otherVolumes.size(); i++ ) {
    outf << "Other Volume " << i << ": " << otherVolumes[i] << std::endl;
  }

  debug() << "************** Process Timing **************" << endmsg;
  outf << std::endl << std::endl << "************** Process Timing **************" << std::endl;
  {
    int proc_indx = 0;
    for ( auto const& proc : sortMapByValue( allProcTimes ) ) {
      outf << "Process " << proc_indx << ": " << proc.second << " cumulated time " << proc.first << " seconds"
           << std::endl;
      proc_indx++;
    }
  }

  // Write out each detector
  for ( auto const& det : knownVolumes.value() ) { WriteSubdetTiming( outf, det ); }

  // List of groups of dets
  std::set<std::string> knownGroups;

  outf << std::endl << "************** Summary of total time in each volume **************" << std::endl;
  for ( auto const& det : knownVolumes.value() ) {
    outf << boost::str( boost::format( "Total time in %s: %.2f seconds (%.2f%% of the total)" ) % det %
                        detsData[det].totalTime % detsData[det].fractionTime )
         << std::endl;

    // Calculate timings for groups
    if ( detsGroups.value().find( det ) != detsGroups.value().end() ) {
      for ( auto const& grp : detsGroups.value()[det] ) {
        knownGroups.insert( grp );
        detsData[grp].totalTime += detsData[det].totalTime;
      }
    }
  }

  // Append time spent in each detector group
  for ( const auto& grp : knownGroups ) {
    detsData[grp].fractionTime = detsData[grp].totalTime * 100 / detsData[detAll].totalTime;
    outf << boost::str( boost::format( "Total time in %s: %.2f seconds (%.2f%% of the total)" ) % grp %
                        detsData[grp].totalTime % detsData[grp].fractionTime )
         << std::endl;
  }
  outf << std::endl;

  outf.close();

  info() << "************** Total Cumulated Time: " << totalCumTime << " **************" << endmsg;
  info() << "************** Total Actual Time: " << totalTimer->GetRealElapsed() << " **************" << endmsg;

  return Print( "Finalization", GiGaStepActionBase::finalize(), MSG::VERBOSE );
}

void MonitorTiming::UserSteppingAction( const G4Step* step ) {
  G4double stepTime = 0;

  if ( allVolsTimes.size() != 0 ) {
    stepTimer->Stop();
    stepTime = stepTimer->GetRealElapsed();
  }

  std::string const volumePath = (std::string)step->GetPreStepPoint()->GetPhysicalVolume()->GetName();

  // Check if the volume is known
  bool volumeIsKnown = false;
  for ( auto const& vol : knownVolumes.value() ) {
    if ( volumePath.find( "/" + vol ) != std::string::npos ) {
      volumeName    = vol;
      volumeIsKnown = true;
      break;
    }
  }

  // Add to OtherVolumes if not a known volume
  if ( !volumeIsKnown ) {
    volumeName = "OtherVolume";
    if ( std::find( otherVolumes.begin(), otherVolumes.end(), volumePath ) == otherVolumes.end() ) {
      otherVolumes.push_back( volumePath );
    }
  }

  // Update all volumes data
  if ( allVolsTimes.find( volumeName ) != allVolsTimes.end() ) {
    allVolsTimes[volumeName] += stepTime;
  } else {
    allVolsTimes.insert( std::pair( volumeName, stepTime ) );
  }

  // Update known volumes data
  int const partId = abs( step->GetTrack()->GetDefinition()->GetPDGEncoding() );
  for ( const auto& det : knownVolumes.value() ) { FillSubdetTiming( partId, stepTime, det ); }

  // Update subvolumes data
  if ( allSubvolsTimes.find( volumePath ) != allSubvolsTimes.end() ) {
    allSubvolsTimes[volumePath] += stepTime;
  } else {
    allSubvolsTimes.insert( std::pair( volumePath, stepTime ) );
  }

  // Update processes data
  std::string const procName = step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
  if ( allProcTimes.find( procName ) != allProcTimes.end() ) {
    allProcTimes[procName] += stepTime;
  } else {
    allProcTimes.insert( std::pair( procName, stepTime ) );
  }

  stepTimer->Start();
}

void MonitorTiming::FillSubdetTiming( int const particleId, double const stepTime, std::string const detName ) {
  if ( detName == volumeName || detName == detAll ) {
    if ( detsData[detName].particlesTimes.find( particleId ) != detsData[detName].particlesTimes.end() ) {
      detsData[detName].particlesTimes[particleId] += stepTime;
    } else {
      detsData[detName].particlesTimes.insert( std::pair( particleId, stepTime ) );
    }
  }
}

void MonitorTiming::WriteSubdetTiming( std::ofstream& out, std::string const detName ) {
  out << std::endl
      << std::endl
      << "************** Timing per particle type in " << detName << " (fraction in " << detName << ") **************"
      << std::endl;

  double time_det = std::accumulate( detsData[detName].particlesTimes.begin(), detsData[detName].particlesTimes.end(),
                                     0., []( double const prev, auto const& el ) { return prev + el.second; } );

  for ( auto const& part : sortMapByValue( detsData[detName].particlesTimes ) ) {
    if ( part.first == 0 ) { continue; }

    G4ParticleDefinition* partDef  = G4ParticleTable::GetParticleTable()->FindParticle( part.second );
    std::string           partName = "";
    if ( partDef ) {
      partName = partDef->GetParticleName();
    } else if ( part.second == 0 ) {
      partName = "opticalphoton";
    } else {
      partName = "unknown";
    }

    if ( partName.size() < 8 ) { partName += "\t"; }
    out << boost::str( boost::format( "%s\t: cumulated time %.2f seconds (%.2f%%)" ) % partName % part.first %
                       ( 100 * part.first / time_det ) )
        << std::endl;
  }

  out << boost::str( boost::format( "Time in %s: %.2f seconds (%.2f%% of the total)" ) % detName % time_det %
                     ( 100 * time_det / totalCumTime ) )
      << std::endl;

  detsData[detName].totalTime    = time_det;
  detsData[detName].fractionTime = 100 * time_det / totalCumTime;
}

std::vector<std::pair<double, std::string>> MonitorTiming::sortMapByValue( std::map<std::string, double> const& data ) {
  std::vector<std::pair<double, std::string>> sorted_data;
  for ( auto const& el : data ) { sorted_data.push_back( std::pair<double, std::string>( el.second, el.first ) ); }
  std::sort( sorted_data.begin(), sorted_data.end(), std::greater<>() );
  return sorted_data;
}

std::vector<std::pair<double, int>> MonitorTiming::sortMapByValue( std::map<int, double> const& data ) {
  std::vector<std::pair<double, int>> sorted_data;
  for ( auto const& el : data ) { sorted_data.push_back( std::pair<double, int>( el.second, el.first ) ); }
  std::sort( sorted_data.begin(), sorted_data.end(), std::greater<>() );
  return sorted_data;
}
