/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaKineRefTable.cpp,v 1.5 2007-10-02 13:14:50 gcorti Exp $
// Include files 

// local
#include "GiGaCnv/GiGaKineRefTable.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GiGaKineRefTable
//
// 2001-07-21 : Vanya Belyaev
//-----------------------------------------------------------------------------

//=============================================================================
// Constructor
//=============================================================================
GiGaKineRefTable::GiGaKineRefTable () : m_table() {}

//=============================================================================
// Destructor
//=============================================================================
GiGaKineRefTable::~GiGaKineRefTable() { m_table.clear(); }

//=============================================================================
