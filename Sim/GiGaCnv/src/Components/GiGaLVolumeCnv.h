/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef     GIGA_GIGALVOLUMECNV_H
#define     GIGA_GIGALVOLUMECNV_H 1

// Include files
// from GiGa
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"
// From LHCb
#include "DetDesc/IDetectorElement.h"

// Forward declarations
class G4LogicalVolume;    ///< Geant4 

 
/** @class GiGaLVolumeCnv GiGaLVolumeCnv.h component/GiGaLVolumeCnv.h 
 *
 *  converter of LVolumes into Geant4 G4LogicalVolume
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @author Sajan Easo, Gloria Corti
 *  @date   Last modified: 2007-07-09
 */

class GiGaLVolumeCnv: public GiGaCnvBase
{

public:

  /** standard constructor
   *  @param Locator pointer to service locator
   */
  GiGaLVolumeCnv( ISvcLocator* Locator );

  /// Standard (virtual) destructor
  virtual ~GiGaLVolumeCnv();

public:

  /** create the representation]
   *  @param Object pointer to object
   *  @param Address address
   *  @return status code
   */
  StatusCode createRep( DataObject*     Object  ,
                        IOpaqueAddress*& Address ) override;

  /** Update representation
   *  @param Object pointer to object
   *  @param Address address
   *  @return status code
   */
  StatusCode updateRep(IOpaqueAddress* Address, DataObject*     Object ) override;

  /// class ID for converted objects
  static const CLID& classID();

  /// storage Type
  static unsigned char storageType();

  /** Provide matrix transformation of physical volume in its mother reference
   *  system with corresponding values from associated detector element
   *  @param IPVolume pointer to physical volume
   *  @param Gaudi::Transform3D matrix transformation
   *  @return status code
   */
  StatusCode transformWithAlignment( const IPVolume* pv,
                                     Gaudi::Transform3D& resultMatrix );

  /** Method to find detector element(s) which are:
   *    - valid
   *    - has geometry
   *    - has logical volume
   *    - corresponds (by name) to a given logical volume
   *    - has AlignmentConditions
   *  It could become a separate utility function.
   *
   *  @param some top-level detector element
   *  @param logical volume name
   *  @param output container
   *  @return int number of detector element for a given Logical Volume
   *
   *  @author Vanya BELYAEV
   *  @date 2007-03-05
   *
   */
  int detElementByLVNameWithAlignment( const DetDesc::IDetectorElementPlus* det,
                                       const std::string& lvName,
                                       std::vector<const DetDesc::IDetectorElementPlus*>& dets);

  /** Method to search in tree the detector element corresponding to a
   *  physical element and check how many physical volumes are associated to it
   *  @param std::string name of physical volume
   *  @param std::vector<DetDesc::IDetectorElementPlus*> pointers to detector elements
   *  @param int number of detector elements found
   *  @return int number of detector element for a given physical volume
   */
  int findBestDetElemFromPVName( std::string pvName,
                                 std::vector<const DetDesc::IDetectorElementPlus*> foundDe,
                                 int& numDetElemFound ) ;

  /* Find the detector element path and fill the variable path.
   * If parent_path is not specified, it is retrieved recursively.
   * @param IDectorElement pointer to detector element of which to find the path
   * @param std::string path in TES of detector element
   * @param std::string path in TES of parent detetor element
   * @return StatusCode
   */
  StatusCode detector_element_support_path(const DetDesc::IDetectorElementPlus *de,
                                           std::string &path,
                                           const std::string &parent_path= "");


private:
  
  GiGaLeaf m_leaf ;   /// Leave in Store

};

#endif  /// GIGA_GIGALVOLUMECNV_H
