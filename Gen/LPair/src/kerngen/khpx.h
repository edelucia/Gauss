/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if 0
* This pilot patch was created from kernhpx.car patch _khpx
*    external names with underscore
*                 IEEE floating point
*                 Hollerith constants exist
*              EQUIVALENCE Hollerith/Character ok
*              Orthodox Hollerith storage left to right
*              Internal double-precision
*               ISA standard routines, ISHFT, IOR, etc
*               MIL standard routines, IBITS, MVBITS, etc
#endif
#ifndef CERNLIB_QMHPX
#define CERNLIB_QMHPX
#endif
#if !defined(CERNLIB_QXNO_SC)
#ifndef CERNLIB_QX_SC
#define CERNLIB_QX_SC
#endif
#endif
#ifndef CERNLIB_QIEEE
#define CERNLIB_QIEEE
#endif
#ifndef CERNLIB_QORTHOLL
#define CERNLIB_QORTHOLL
#endif
#ifndef CERNLIB_INTDOUBL
#define CERNLIB_INTDOUBL
#endif
#ifndef CERNLIB_QISASTD
#define CERNLIB_QISASTD
#endif
#ifndef CERNLIB_QMILSTD
#define CERNLIB_QMILSTD
#endif
#ifndef CERNLIB_QS_UNIX
#define CERNLIB_QS_UNIX
#endif
#ifndef CERNLIB_QGETCWD
#define CERNLIB_QGETCWD
#endif
#ifndef CERNLIB_QSIGPOSIX
#define CERNLIB_QSIGPOSIC
#endif
#ifndef CERNLIB_QINTZERO
#define CERNLIB_QINTZERO
#endif
