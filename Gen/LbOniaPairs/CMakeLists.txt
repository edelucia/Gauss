###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbOniaPairs
---------------
#]=======================================================================]

gaudi_add_library(LbOniaPairsLib
    SOURCES
        src/Lib/OniaPairs.cpp
        src/Lib/oniapairs_upevnt.F
        src/Lib/oniapairs_upinit.F
        src/Lib/oniapairsgetpar.F
        src/Lib/oniapairsprintx.F
        src/Lib/oniapairssetpar.F
    LINK
        PUBLIC
            Gauss::pythia6forgauss
            Gauss::LbPythiaLib
)

gaudi_add_module(LbOniaPairs
    SOURCES
        src/Component/OniaPairsProduction.cpp
    LINK
        Gauss::LbOniaPairsLib
)
