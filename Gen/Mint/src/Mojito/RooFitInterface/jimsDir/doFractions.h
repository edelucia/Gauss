/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:08 GMT
#ifndef DOFRACTIONS_H
#define DOFRACTIONS_H

#include "RooDataSet.h"
#include "ComplexSum.h"

#include <string>
#include <map>

std::map<std::string, Double_t> doFractions(const ComplexSum& comsum
					    , RooDataSet* data
					    , int maxEvents=-9999
					    );
#endif
