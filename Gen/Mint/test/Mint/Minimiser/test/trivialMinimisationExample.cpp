/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT
#include "Mint/FitParameter.h"
#include "Mint/Minimiser.h"
#include "Mint/Minimisable.h"

using namespace MINT;

class toyFitFun : public Minimisable{
  FitParameter fp;
public:
  toyFitFun() : fp("x"){}
  double getVal(){return (fp - 4.)*(fp - 4.);}
};


int trivialMinimisationExample(){

  toyFitFun f;

  Minimiser mini(&f);
  mini.doFit();

  return 0;
}
//
