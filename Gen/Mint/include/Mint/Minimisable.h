/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINIMISABLE_HH
#define MINIMISABLE_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT

#include "Mint/IMinimisable.h"
#include "Mint/MinuitParameterSet.h"

namespace MINT{
class Minimisable : virtual public IMinimisable{
  MinuitParameterSet* _pset;
 public:
  Minimisable(MinuitParameterSet* mps=0);
  Minimisable(const Minimisable& other);
  void setPset(MinuitParameterSet* mps);
  MinuitParameterSet* getParSet() override;


  // you can choose to implement these:
  void beginFit() override {}
  void parametersChanged() override {}
  void endFit() override {}

 // you'll have to implement this:
  double getVal() override =0;

  // useful:
  double getNewVal() override {
    parametersChanged();
    return getVal();
  }
};
}//namespace MINT

#endif
//
