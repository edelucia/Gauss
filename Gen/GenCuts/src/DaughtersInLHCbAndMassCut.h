/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaughtersInLHCbAndMassCut.h,v 1.4 2008-05-29 14:21:59 gcorti Exp $
#ifndef GENCUTS_DAUGHTERSINLHCBANDMASSCUT_H
#define GENCUTS_DAUGHTERSINLHCBANDMASSCUT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class DaughtersInLHCbAndMassCut DaughtersInLHCbAndMassCut.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class DaughtersInLHCbAndMassCut : public extends<GaudiTool,IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin", 10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax", 400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin", 5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax", 400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  Gaudi::Property<int> m_LeptonOneID{this,"LeptonOneID", -11,"LeptonOneID"};
  Gaudi::Property<int> m_LeptonTwoID{this,"LeptonTwoID",  11,"LeptonTwoID"};
  Gaudi::Property<int> m_HadronOneABSID{this,"HadronOneABSID", 321,"HadronOneABSID"};
  Gaudi::Property<int> m_HadronTwoABSID{this,"HadronTwoABSID", 211,"HadronTwoABSID"};

  Gaudi::Property<double> m_DausPTMin{this,"DausPTMin", 0.2 * Gaudi::Units::GeV,"DausPTMin"};
  Gaudi::Property<double> m_eeMassMax{this,"eeMassMax", 1.5 * Gaudi::Units::GeV,"ee Mass Max"};
  Gaudi::Property<double> m_eeKstarMassMin{this,"eeKstarMassMin", 4.0 * Gaudi::Units::GeV,"eeKstar Mass Min"};


};
#endif // GENCUTS_DAUGHTERSINLHCB_H
