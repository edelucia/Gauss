/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaughtersInLHCbAndWithKSCutsAndWithMinPAndPT.h,v 1.4 2008-05-29 14:21:59 gcorti Exp $
#ifndef GENCUTS_DAUGHTERSINLHCBAndWithDaughAndBCuts_H
#define GENCUTS_DAUGHTERSINLHCBAndWithDaughAndBCuts_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class DaughtersInLHCbAndWithDaughAndBCuts DaughtersInLHCbAndWithDaughAndBCuts.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb AndWithMinP.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Alex Shires
 *  @date   2011-03-02
 */
class DaughtersInLHCbAndWithDaughAndBCuts : public extends<GaudiTool,IGenCutTool> {
 public:
  using extends::extends;

  /** Accept events with daughters in LHCb AndWithMinP (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Flight Distance Cut function
   *
   */
  bool flightCut( const HepMC::GenParticle * , double ) const ;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC::GenParticle *p, const double pMin, double& sumP) const ;

  /** Transverse Momentum Cut function
   *
   */
  bool transverseMomentumCut( const HepMC::GenParticle *p, const double pTMin, double& sumPt) const ;

  // Minimum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  // Maximum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  // Minimum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  // Maximum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  // Minimum value of angle around z-axis for a KS/Lambda
  Gaudi::Property<double> m_llThetaMin{this,"LongLivedThetaMin",0 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for a KS/Lambda"} ;

  // Maximum value of angle around z-axis for a KS/Lambda
  Gaudi::Property<double> m_llThetaMax{this,"LongLivedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for a KS/Lambda"} ;

  //min value of momentum
  Gaudi::Property<double> m_minMuonP{this,"MinMuonP",0 * Gaudi::Units::MeV,"min value of muon momentum"};
  Gaudi::Property<double> m_minTrackP{this,"MinTrackP",0 * Gaudi::Units::MeV,"min value of track momentum"};
  Gaudi::Property<double> m_minLongLivedP{this,"MinLongLivedP",0 * Gaudi::Units::MeV,"min value of long lived momentum"};
	Gaudi::Property<double> m_minLongLivedDaughP{this,"MinLongLivedDaughP",0 * Gaudi::Units::MeV,"min value of long lived child momentum"};
  Gaudi::Property<double> m_minBP{this,"MinBP",0 * Gaudi::Units::MeV,"min value of B momentum"};

  //min value of transverse momentum
  Gaudi::Property<double> m_minMuonPT{this,"MinMuonPT",0 * Gaudi::Units::MeV,"min value of muon transverse momentum"};
  Gaudi::Property<double> m_minTrackPT{this,"MinTrackPT",0 * Gaudi::Units::MeV,"min value of track transverse momentum"};
  Gaudi::Property<double> m_minLongLivedPT{this,"MinLongLivedPT",0 * Gaudi::Units::MeV,"min value of long lived transverse momentum"};
  Gaudi::Property<double> m_minLongLivedDaughPT{this,"MinLongLivedDaughPT",0 * Gaudi::Units::MeV,"min value of long lived child transverse momentum"};
  Gaudi::Property<double> m_minBPT{this,"MinBPT",0 * Gaudi::Units::MeV,"min value of B transverse momentum"};

  //flight distance
  Gaudi::Property<double> m_minBFD{this,"MinBFD",0 * Gaudi::Units::mm,"B flight distance"};

	// sum of Pt and P
	Gaudi::Property<double> m_minSumP{this,"MinSumP",0* Gaudi::Units::MeV,"Min Sum of P"};
  Gaudi::Property<double> m_minSumPT{this,"MinSumPT",0* Gaudi::Units::MeV,"Min Sum of PT"};


};
#endif // GENCUTS_DAUGHTERSINLHCBAndWithDaughAndBCuts_H
