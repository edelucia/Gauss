###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/GENSER
----------
#]=======================================================================]

string(REPLACE "-Wl,--no-undefined" "" CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}")
string(APPEND CMAKE_Fortran_FLAGS " -fsecond-underscore")

gaudi_add_library(pythia6forgauss
    SOURCES
        src/pythia6/pyevwt.F
        src/pythia6/pyinit.F
        src/pythia6/pykfdi.F
        src/pythia6/pyr.F
        src/pythia6/pyrand.F
        src/pythia6/pyscat.F
        src/pythia6/pysghf.F
        src/pythia6/pysigh.F
        src/pythia6/upevnt.F
        src/pythia6/upinit.F
        src/pythia6/upveto.F
        src/pythia6/xsectionPsiStar.F
    LINK
        PUBLIC
            Pythia6::pythia6
            Pythia6::pythia6_dummy
            LHAPDF::LHAPDF
)

# TODO: [New CMake] is it needed?
set_property(TARGET pythia6forgauss
             APPEND PROPERTY LINK_FLAGS ${PYTHIA6_LIBRARY_DIRS}/pydata.o)
