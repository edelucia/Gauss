/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MIBackground.h,v 1.1 2007-08-17 12:54:14 gcorti Exp $
#ifndef MIBACKGROUND_H
#define MIBACKGROUND_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// LHCb
#include "Event/GenHeader.h"

#include "IMIBSource.h"
//Kernel
#include "GaudiKernel/SystemOfUnits.h"


/** @class MIBackground MIBackground.h
 *
 *  Algorithm to read file containing particles reaching LHCb cavern
 *  and due to halo impinging on the Tertiary Collimators.
 *  The source can be re-weighted according to the options selected
 *  and produces particles from it.
 *
 *  @author Magnus Lieng
 *  @date   2006-10-10
 */
class MIBackground : public GaudiAlgorithm {
public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  /// Generate an "event" and put information in containers
  StatusCode generate( LHCb::GenHeader* theHeader,
                       LHCb::GenCollisions* theCollisions,
                       LHCb::HepMCEvents* theEvents );

private:

  Gaudi::Property<std::string> m_eventLoc{this,
  "HepMCEventLocation",LHCb::HepMCEventLocation::Default,
  "TES location of HepMCEvent"};///< TES location of HepMCEvent
  Gaudi::Property<std::string> m_headerLoc{this,
  "GenHeaderLocation",LHCb::GenHeaderLocation::Default,
  "TES location of GenHeader"};///< TES location of GenHeader
  Gaudi::Property<std::string> m_collLoc{this,
  "GenCollisionLocation",LHCb::GenCollisionLocation::Default,
  "TES location of Collisions"};///< TES location of Collisions

  Gaudi::Property<double> m_luminosity{this,
  "Luminosity",2.e32 /Gaudi::Units::cm2/Gaudi::Units::s,
  "Luminosity to store in TES"}; ///< Luminosity to store in TES
  Gaudi::Property<int> m_evtType{this,
  "EventType",0,
  "Event type"}; ///< Event type

  Gaudi::Property<std::vector<std::string>> m_sourcesNames{this,
    "MIBSources",{},
  "type/name of (tools) sources"};  ///< type/name of (tools) sources
  std::vector<IMIBSource*> m_sources{};       ///< pointers to (tools) sources

};

#endif  // MIBACKGROUND_H

