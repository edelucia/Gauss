/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Header file
#include "LbEvtGen/EvtGenDecayWithCutTool.h"

// Include files
#include <fstream>
#include <iostream>

// from LHCb
#include "GenEvent/HepMCUtils.h"

// from HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

#include "MCInterfaces/IGenCutTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : EvtGenDecayWithCutTool
//
// Dominik Muller
// 2018-3-12
//-----------------------------------------------------------------------------


//=============================================================================
// Initialize method
//=============================================================================
StatusCode EvtGenDecayWithCutTool::initialize()
{
  StatusCode sc = EvtGenDecay::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_cutToolName.value() != "" ) m_cutTool = tool<IGenCutTool>( m_cutToolName.value(), this );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Generate a Decay tree from a particle theMother in the event theEvent
// Repeat until it passes the the cut;
//=============================================================================
StatusCode EvtGenDecayWithCutTool::generateSignalDecay( HepMC::GenParticle* theMother, bool& flip ) const
{
  int counter    = 0;
  bool found_one = false;
  while ( !found_one ) {
    HepMCUtils::RemoveDaughters( theMother );
    StatusCode sc = EvtGenDecay::generateSignalDecay( theMother, flip );
    if ( sc.isFailure() ) {
      return sc;
    }

    IGenCutTool::ParticleVector theParticleList;
    theParticleList.push_back( theMother );
    if ( m_cutTool ) {
      // Passing nullptr for lack of anything else here.
      // Just hope the cut tool is not using those
      found_one = m_cutTool->applyCut( theParticleList, nullptr, nullptr );
    } else {
      // Skip this if no cut tool was provided
      found_one = true;
    }
    counter += 1;
  }
  debug() << "Needed " << counter << " attempts to get one that passes the cut." << endmsg;
  return StatusCode::SUCCESS;
}
