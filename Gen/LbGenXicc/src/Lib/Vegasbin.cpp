/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// access GenXicc common VEGASBIN 
#include "LbGenXicc/Vegasbin.h"

// set pointer to zero at start
Vegasbin::VEGASBIN* Vegasbin::s_vegasbin =0;

// Constructor
Vegasbin::Vegasbin() { }

// Destructor
Vegasbin::~Vegasbin() { }

// access nvbin in common
int& Vegasbin::nvbin() {
  init(); // check COMMON is initialized
  return s_vegasbin->nvbin;
}
