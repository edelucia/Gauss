/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatPtRapidity.cpp,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $

// This class
#include "FlatPtRapidity.h"

// From STL
#include <cmath>

// FromGaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IRndmGenSvc.h" 
#include "TRandom3.h"
#include "Event/GenHeader.h"

//===========================================================================
// Implementation file for class: FlatPtRapidity
//
// 2016-02-19: Dan Johnson
//===========================================================================

DECLARE_COMPONENT( FlatPtRapidity )


//===========================================================================
// Initialize Particle Gun parameters
//===========================================================================
StatusCode FlatPtRapidity::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatGenerator.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Cannot initialize flat generator" ) ;
 
  // Get the mass of the particle to be generated
  //
  LHCb::IParticlePropertySvc* ppSvc = 
    svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" , true ) ;

  m_masses.clear();

  // If trying to sample mass but meaningless mass range, throw an error
  if (m_sampleMass.value()) {
    if (m_MassRange_min.value()<0. || m_MassRange_max.value()<0. || m_MassRange_min.value() > m_MassRange_max.value() )  {
      debug() << "==> Min: " << m_MassRange_min.value() << endmsg ;
      debug() << "==> Max: " << m_MassRange_max.value() << endmsg ;
      return Error( "You asked to sample the particle mass, but defined a meaningless mass range" ) ;
    }
  }
    
  // check pt and rapidity
  if ( ( m_minPt.value()         > m_maxPt.value() ) || ( m_minRapidity.value() > m_maxRapidity.value() ) )
    return Error( "Incorrect values for pT or rapidity" ) ;
  
  // setup particle information
  info() << "Particle type chosen randomly from :" << endmsg;
  PIDs::iterator icode ;
  for ( icode = m_pdgCodes.begin(); icode != m_pdgCodes.end(); ++icode ) {
    const LHCb::ParticleProperty * particle = ppSvc->find( LHCb::ParticleID( *icode ) );
    m_masses.push_back( ( particle->mass() ) ) ;
    m_names.push_back( particle->particle() ) ;
    info() << " " << particle->particle() ;
  }
  
  info() << endmsg ;

  info() << "pT range: " << m_minPt.value() / Gaudi::Units::GeV << " GeV <-> " 
          << m_maxPt.value() / Gaudi::Units::GeV << " GeV" << endmsg ;
  info() << "Rapidity range: " << m_minRapidity.value() << " <-> " 
          << m_maxRapidity.value() << endmsg ;
  
  release( ppSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return sc ;
}

//===========================================================================
// Generate the particles
//===========================================================================
void FlatPtRapidity::generateParticle( Gaudi::LorentzVector & momentum , 
                                      Gaudi::LorentzVector & origin , 
                                      int & pdgId ) {  
  
  origin.SetCoordinates( 0. , 0. , 0. , 0.  );                                      
  
  // randomly choose a particle type
  unsigned int currentType = 
    (unsigned int)( m_pdgCodes.size() * m_flatGenerator() );
  // protect against funnies
  if ( currentType >= m_pdgCodes.size() ) currentType = 0; 
 
  double px(0.), py(0.), pz(0.) ;
  // Generate values for energy, theta and phi
  double pt       = m_minPt.value() + m_flatGenerator() * (m_maxPt.value()-m_minPt.value()) ;
  double rapidity = m_minRapidity.value() + m_flatGenerator() * (m_maxRapidity.value()-m_minRapidity.value()) ;
  
  // Take particle mass from ParticlePropertyService according to pdg id being generated
  // If sampling the mass, change the energy of the particle appropriately
  double mass = -1. ;
  if (m_sampleMass.value()) {
    mass = m_MassRange_min.value() + m_flatGenerator() * (m_MassRange_max.value()-m_MassRange_min.value()) ;
  }
  else {
    mass = m_masses[currentType] ;
  }
 
  // Transform to x,y,z coordinates
  pz = sqrt( ( mass*mass + pt*pt ) ) * (exp(2*rapidity) - 1) / ( 2 * exp(rapidity) ) ;

  double phi = (-1.*Gaudi::Units::pi + m_flatGenerator() * Gaudi::Units::twopi) * Gaudi::Units::rad;
  px = pt*cos(phi);
  py = pt*sin(phi);
    
  momentum.SetPx( px ) ; momentum.SetPy( py ) ; momentum.SetPz( pz ) ;
  momentum.SetE( std::sqrt( mass * mass + 
                            momentum.P2() ) ) ;  
  debug() << "Sampled mass:     " << mass << " and four-momentum mass:     " << momentum.M() << endmsg ;
  debug() << "Sampled pt:       " << pt << " and four-momentum pt:       " << momentum.pt() << endmsg ;
  debug() << "Sampled rapidity: " << rapidity << " and four-momentum rapidity: " << momentum.Rapidity() << endmsg ;
                        
  pdgId = m_pdgCodes[ currentType ] ;
    
  debug() << " -> " << m_names[ currentType ] << endmsg 
          << "   P   = " << momentum << endmsg ;
}

