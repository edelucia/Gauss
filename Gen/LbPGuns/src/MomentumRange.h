/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MomentumRange.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_MOMENTUMRANGE_H
#define PARTICLEGUNS_MOMENTUMRANGE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"
#include "GaudiKernel/RndmGenerators.h"

/** @class MomentumRange MomentumRange.h "MomentumRange.h"
 *
 *  Particle gun with given momentum range
 *
 *  @author Patrick Robbe
 *  @date   2008-05-18
 */
class MomentumRange : public extends<GaudiTool, IParticleGunTool> {
 public:

  /// Constructor
  using extends::extends;

  /// Initialize particle gun parameters
  StatusCode initialize() override;

  /// Generation of particles
  void generateParticle( Gaudi::LorentzVector & momentum ,
                         Gaudi::LorentzVector & origin ,
                         int & pdgId ) override;

  /// Print counters
  void printCounters( ) override { ; } ;

 private:
  Gaudi::Property<double> m_minMom{this,"MomentumMin",100.0 * Gaudi::Units::GeV,"Minimum momentum "};   ///< Minimum momentum (Set by options)
  Gaudi::Property<double> m_minTheta{this,"ThetaMin",0.1 * Gaudi::Units::rad,"Minimum theta angle"}; ///< Minimum theta angle (Set by options)
  Gaudi::Property<double> m_minPhi{this,"PhiMin",0. * Gaudi::Units::rad,"Minimum phi angle"};   ///< Minimum phi angle (Set by options)

  Gaudi::Property<double> m_maxMom{this,"MomentumMax",100.0 * Gaudi::Units::GeV,"Maximum momentum "};   ///< Maximum momentum (Set by options)
  Gaudi::Property<double> m_maxTheta{this,"ThetaMax",0.4 * Gaudi::Units::rad,"Maximum theta angle "}; ///< Maximum theta angle (Set by options)
  Gaudi::Property<double> m_maxPhi{this,"PhiMax",Gaudi::Units::twopi * Gaudi::Units::rad,"Maximum phi angle "};   ///< Maximum phi angle (Set by options)

  /// Pdg Codes of particles to generate (Set by options)
  Gaudi::Property<std::vector<int>> m_pdgCodes{this,"PdgCodes",{-211},"Pdg Codes of particles to generate"};

  /// Masses of particles to generate
  std::vector<double>      m_masses{};

  /// Names of particles to generate
  std::vector<std::string> m_names{};

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator ;
};

#endif // PARTICLEGUNS_MOMENTUMRANGE_H
