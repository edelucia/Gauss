/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Cosmics.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_COSMICS_H
#define PARTICLEGUNS_COSMICS_H 1

// Include Files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h" 
#include "GaudiKernel/PhysicalConstants.h"

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

/** @class Cosmics Cosmics.h "Cosmics.h"
 *
 *  Tool to generate cosmic particles inside LHCb detector
 *
 *  @author Giulia Manca
 *  @date   2008-05-18
 */

class Cosmics : public extends<GaudiTool, IParticleGunTool> {
 public:

  /// Constructor
  using extends::extends;

  /// Initialize particle gun parameters
  StatusCode initialize() override;

  /// Generation of particles
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin , int & pdgId ) override;


  /// Print counters
  void printCounters( ) override { ; } ;

 private:

  /// Flux function
  double muonSpec(double muMomentum, double Theta, double vDepth,
                              double overAngle);

  /// Setable Properties:-

  /// Minimum momentum of the mu at detection
  Gaudi::Property<double> m_minMom{this,"MomentumMin",   1000.0 * Gaudi::Units::MeV,"Minimum momentum of the mu at detection"};

  /// Minimum zenith angle of the cosmic ray
  Gaudi::Property<double> m_minTheta{this,"ThetaMin", 0.1 * Gaudi::Units::rad,"Minimum zenith angle of the cosmic ray"};

  /// Minimum phi angle
  Gaudi::Property<double> m_minPhi{this,"PhiMin", 0. * Gaudi::Units::rad,"Minimum phi angle"};

  /// Maximum momentum of the mu at detection
  Gaudi::Property<double> m_maxMom{this,"MomentumMax", 100000.0 * Gaudi::Units::MeV,"Maximum momentum of the mu at detection"};

  /// Maximum zenith Angle of the muon
  Gaudi::Property<double> m_maxTheta{this,"ThetaMax", 1.5708 * Gaudi::Units::rad,"Maximum zenith Angle of the muon"};

  /// Maximum phi angle
  Gaudi::Property<double> m_maxPhi{this,"PhiMax", Gaudi::Units::twopi* Gaudi::Units::rad,"Maximum phi angle"};
  
  //xyz define the surface of the detector I want to
  //uniformly populate. The vertex is then extrapolated to
  //a plane above the detector at y=+6m.
  //default is the all detector at zero

  /// Minimum x position of the point of interaction of the muon
  /// with the detector; the vertex position is then extrapolated
  Gaudi::Property<double> m_minxvtx{this,"xMin",-5000.0 * Gaudi::Units::mm ,"Minimum x position of the point of interaction of the muon "
"with the detector; the vertex position is then extrapolated"};

  /// Maximum x position of the point of interaction of the muon
  /// with the detector; the vertex position is then extrapolated
  Gaudi::Property<double> m_maxxvtx{this,"xMax", 5000.0 * Gaudi::Units::mm ,"Maximum x position of the point of interaction of the muon "
"with the detector; the vertex position is then extrapolated"};

  /// Minimum y position of the point of interaction of the muon
  /// with the detector; the vertex position is then extrapolated
  Gaudi::Property<double> m_minyvtx{this,"yMin", -5000.0 * Gaudi::Units::mm ,"Minimum y position of the point of interaction of the muon "
"with the detector; the vertex position is then extrapolated"};


  /// Maximum y position of the point of interaction of the muon
  /// with the detector; the vertex position is then extrapolated
  Gaudi::Property<double> m_maxyvtx{this,"yMax", 5000.0 * Gaudi::Units::mm ,"Maximum y position of the point of interaction of the muon "
"with the detector; the vertex position is then extrapolated"};

  /// z position of the point of interaction of the muon
  /// with the detector; the vertex position is then extrapolated
  Gaudi::Property<double> m_zvtx{this,"z",0.0 * Gaudi::Units::mm,"z position of the point of interaction of the muon "
"with the detector; the vertex position is then extrapolated"};

  ///Time I want the mu to interact; then the real
  ///time of the vertex is extrapolated
  Gaudi::Property<double> m_time{this,"TimeOfInteraction",0.0 * Gaudi::Units::s,"Time I want the mu to interact; then the real "
"time of the vertex is extrapolated"};

  /// Theory model
  Gaudi::Property<int> m_model{this,"TheoryModel",0,"possible to choose between 1=nucl-ex/0601019 and 0=hep-ph/0604145; "
"default is 0; model 1 is strongly discouraged"};

  /// Pdg Codes of particles to generate
  Gaudi::Property<std::vector<int>> m_pdgCodes{this,"PdgCodes",{13,-13},"Pdg Codes of particles to generate"};

  /// Masses of particles to generate
  std::vector<double>      m_masses{};

  /// Names of particles to generate
  std::vector<std::string> m_names{};

  /// Flat random number generator
  Rndm::Numbers m_flatGenerator ;
};

#endif // PARTICLEGUNS_COSMICS_H
