/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedLuminosityForSpillOver.h,v 1.2 2009-04-07 16:11:21 gcorti Exp $
#ifndef GENERATORS_FIXEDLUMINOSITYFORSPILLOVER_H
#define GENERATORS_FIXEDLUMINOSITYFORSPILLOVER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IPileUpTool.h"

// From Event
#include "Event/BeamParameters.h"
#include "Event/GenFSR.h"

// forward declaration
class IRndmGenSvc ;
class ICounterLogFile ;

/** @class FixedLuminosityForSpillOver FixedLuminosityForSpillOver.h "FixedLuminosityForSpillOver.h"
 *
 *  Tool to compute variable number of pile up events
 *  depending on beam parameters.
 *  To be used for spill over generation because it accepts events
 *  with 0 interaction
 *
 *  @author Patrick Robbe
 *  @date   2009-03-30
 */
class FixedLuminosityForSpillOver : public extends<GaudiTool,IPileUpTool> {
public:
  /// Standard constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize method
  StatusCode finalize( ) override;

  /** Implements IPileUpTool::numberOfPileUp
   *  Returns the number of pile-up interactions in one event. It follows
   *  a Poisson distribution with
   *  mean = Luminosity * cross_section / crossing_rate.
   *  The fixed luminosity is returned as the currentLuminosity.
   */
  unsigned int numberOfPileUp( ) override;

  /// Implements IPileUpTool::printPileUpCounters
  void printPileUpCounters( ) override;

protected:

private:
  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string>  m_FSRName{this,"GenFSRLocation",LHCb::GenFSRLocation::Default,"GenFSRLocation"};

  ICounterLogFile * m_xmlLogTool{nullptr} ; ///< Log file in XML

  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  int    m_numberOfZeroInteraction{0} ; ///< Counter of empty events

  int    m_nEvents{0} ; ///< Counter of events (including empty events)

  IRndmGenSvc * m_randSvc{nullptr} ; ///< Pointer to random number generator service
};
#endif // GENERATORS_FIXEDLUMINOSITYFORSPILLOVER_H
