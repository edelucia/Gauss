/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: VariableLuminosity.cpp,v 1.6 2009-04-07 16:11:21 gcorti Exp $
// Include files

// local
#include "VariableLuminosity.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"

// From CLHEP
#include "CLHEP/Units/SystemOfUnits.h"

// From Event
#include "Event/GenCountersFSR.h"

// From Generators
#include "Generators/GenCounters.h"
#include "Generators/ICounterLogFile.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VariableLuminosity
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( VariableLuminosity )


//=============================================================================
// Initialize method
//=============================================================================
StatusCode VariableLuminosity::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // Initialize the number generator
  m_randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;

  // XML file
  m_xmlLogTool = tool< ICounterLogFile >( "XmlCounterLogFile" ) ;

  sc = m_flatGenerator.initialize( m_randSvc , Rndm::Flat( 0 , 1 ) ) ;
  if ( ! sc.isSuccess() )
    return Error( "Could not initialize flat random generator" ) ;

  using CLHEP::s;
  info() << "Poisson distribution with 'LHCb mean'. " << endmsg ;
  info() << "Fill duration (hours): " << m_fillDuration.value() / 3600 / s << endmsg ;
  info() << "Beam decay time (hours): " << m_beamDecayTime.value() / 3600 / s
         << endmsg ;

  return sc ;
}

//=============================================================================
// Compute the number of pile up to generate according to beam parameters
//=============================================================================
unsigned int VariableLuminosity::numberOfPileUp( ) {
  LHCb::BeamParameters * beam = get< LHCb::BeamParameters >( m_beamParameters.value() ) ;
  if ( 0 == beam ) Exception( "No beam parameters registered" ) ;

  LHCb::GenFSR* genFSR = nullptr;
  if(m_FSRName.value() != ""){
    IDataProviderSvc* fileRecordSvc = svc<IDataProviderSvc>("FileRecordDataSvc", true);
    genFSR = getIfExists<LHCb::GenFSR>(fileRecordSvc, m_FSRName.value(), false);
    if(!genFSR) warning() << "Could not find GenFSR at " << m_FSRName.value() << endmsg;
  }

  unsigned int result = 0 ;
  double mean , currentLuminosity;
  while ( 0 == result ) {
    m_nEvents++ ;
    if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::AllEvt, 1);
    currentLuminosity = beam -> luminosity() * m_fillDuration.value() / m_beamDecayTime.value() /
      ( 1.0 - exp( -m_fillDuration.value() / m_beamDecayTime.value() ) ) ;

    mean = currentLuminosity * beam -> totalXSec() / beam -> revolutionFrequency() ;
    Rndm::Numbers poissonGenerator( m_randSvc , Rndm::Poisson( mean ) ) ;
    result = (unsigned int) poissonGenerator() ;
    if ( 0 == result ) {
      m_numberOfZeroInteraction++ ;
      if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::ZeroInt, 1);
    }
  }

  return result ;
}

//=============================================================================
// Print the specific pile up counters
//=============================================================================
void VariableLuminosity::printPileUpCounters( ) {
  using namespace GenCounters ;
  printCounter( m_xmlLogTool , "all events (including empty events)" , m_nEvents ) ;
  printCounter( m_xmlLogTool , "events with 0 interaction" ,
                m_numberOfZeroInteraction ) ;
}

//=============================================================================
// Finalize method
//=============================================================================
StatusCode VariableLuminosity::finalize( ) {
  release( m_randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return GaudiTool::finalize( ) ;
}
