/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatSmearVertex.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_FLATSMEARVERTEX_H
#define PARTICLEGUNS_FLATSMEARVERTEX_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/PhysicalConstants.h"

#include "Generators/IVertexSmearingTool.h"

/** @class FlatSmearVertex FlatSmearVertex.h "FlatSmearVertex.h"
 *
 *  Tool to smear vertex with flat smearing along the x- y- and z-axis.
 *  Concrete implementation of a IVertexSmearingTool.
 *
 *  @author Patrick Robbe
 *  @date   2008-05-18
 */
class FlatSmearVertex : public extends<GaudiTool,IVertexSmearingTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /// Initialize method
  StatusCode initialize( ) override;

  /** Implements IVertexSmearingTool::smearVertex.
   */
  StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

 private:
  /// Minimum value for the x coordinate of the vertex (set by options)
  Gaudi::Property<double> m_xmin{this,"xVertexMin",0.0 * Gaudi::Units::mm,"xVertexMin"}   ;

  /// Minimum value for the y coordinate of the vertex (set by options)
  Gaudi::Property<double> m_ymin{this,"yVertexMin",0.0 * Gaudi::Units::mm,"yVertexMin"}   ;

  /// Minimum value for the z coordinate of the vertex (set by options)
  Gaudi::Property<double> m_zmin{this,"zVertexMin",0.0 * Gaudi::Units::mm,"zVertexMin"}   ;

  /// Maximum value for the x coordinate of the vertex (set by options)
  Gaudi::Property<double> m_xmax{this,"xVertexMax",0.0 * Gaudi::Units::mm,"xVertexMax"}   ;

  /// Maximum value for the y coordinate of the vertex (set by options)
  Gaudi::Property<double> m_ymax{this,"yVertexMax",0.0 * Gaudi::Units::mm,"yVertexMax"}   ;

  /// Maximum value for the z coordinate of the vertex (set by options)
  Gaudi::Property<double> m_zmax{this,"zVertexMax",0.0 * Gaudi::Units::mm,"zVertexMax"}   ;

  /// Direction of the beam to take into account TOF vs nominal IP8, can have
  /// only values -1 or 1, or 0 to switch off the TOF and set time of
  /// interaction to zero (default = 1, as for beam 1)
  Gaudi::Property<int> m_zDir{this,"BeamDirection",0,"BeamDirection"};

  Gaudi::Property<bool> m_tilt{this,"Tilt",false,"Tilt"};
  Gaudi::Property<double> m_tiltAngle{this,"TiltAngle",-3.601e-3,"TiltAngle"};

  Rndm::Numbers m_flatDist ; ///< Flat random number generator
};
#endif // PARTICLEGUNS_FLATSMEARVERTEX_H
