/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FlatZSmearVertex.cpp,v 1.14 2010-05-09 17:05:24 gcorti Exp $
// Include files 

// local
#include "FlatZSmearVertex.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h" 
#include "GaudiKernel/Vector4DTypes.h"

// from Event
#include "Event/HepMCEvent.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FlatZSmearVertex
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( FlatZSmearVertex )

//=============================================================================
// Initialize 
//=============================================================================
StatusCode FlatZSmearVertex::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;
  
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Could not initialize gaussian random number generator" ) ;
  if ( m_zmin.value() > m_zmax.value() ) return Error( "zMin > zMax !" ) ;
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( m_zmin.value() , m_zmax.value() ) ) ;
  if ( ! sc.isSuccess() ) 
    return Error( "Could not initialize flat random number generator" ) ;

  std::string infoMsg = " applying TOF of interaction with ";
  if ( m_zDir.value() == -1 ) {
    infoMsg = infoMsg + "negative beam direction";
  } else if ( m_zDir.value() == 1 ) {
    infoMsg = infoMsg + "positive beam direction";
  } else if ( m_zDir.value() == 0 ) {
    infoMsg = " with TOF of interaction equal to zero ";
  } else {
    return Error("BeamDirection can only be set to -1 or 1, or 0 to switch off TOF");
  }

  info() << "Smearing of interaction point with transverse Gaussian "
         << " distribution " << endmsg;
  info() << infoMsg << endmsg;
  info() << "and flat longitudinal z distribution" << endmsg;

  release( randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
 
  return sc ;
}
 
//=============================================================================
// Smearing function
//=============================================================================
StatusCode FlatZSmearVertex::smearVertex( LHCb::HepMCEvent * theEvent ) {

  LHCb::BeamParameters * beam = get< LHCb::BeamParameters >( m_beamParameters.value() ) ;
  if ( 0 == beam ) Exception( "No beam parameters registered" ) ;

  double dx , dy , dz , dt;
  
  dz = m_flatDist( ) ;
  dt = m_zDir.value() * dz/Gaudi::Units::c_light;

  do { dx = m_gaussDist( ) ; } while ( fabs( dx ) > m_xcut.value() ) ;
  dx = dx * beam -> sigmaX() * sqrt( 2. ) ;
  do { dy = m_gaussDist( ) ; } while ( fabs( dy ) > m_ycut.value() ) ;
  dy = dy * beam -> sigmaY() * sqrt( 2. ) ;

  // take into account mean at z=0 and crossing angle
  dx = dx/cos( beam -> horizontalCrossingAngle() ) + 
    beam -> beamSpot().x() + dz*sin( beam -> horizontalCrossingAngle() )*m_zDir.value();
  dy = dy/cos( beam -> verticalCrossingAngle() ) + 
    beam -> beamSpot().y() + dz*sin( beam -> verticalCrossingAngle() )*m_zDir.value();

  Gaudi::LorentzVector dpos( dx , dy , dz , dt ) ;
  
  HepMC::GenEvent::vertex_iterator vit ;
  HepMC::GenEvent * pEvt = theEvent -> pGenEvt() ;
  for ( vit = pEvt -> vertices_begin() ; vit != pEvt -> vertices_end() ; 
        ++vit ) {
    Gaudi::LorentzVector pos ( (*vit) -> position() ) ;
    pos += dpos ;
    (*vit) -> set_position( HepMC::FourVector( pos.x() , pos.y() , 
                                               pos.z() , pos.t() ) ) ;
  }

  return StatusCode::SUCCESS ;      
}

